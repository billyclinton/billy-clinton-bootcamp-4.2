import { Injectable } from '@angular/core';

@Injectable()
export class CourseService {

  constructor() { }
CourseList:Object[]=[
    {"id":"1","name":"Math", "lecturer":"Mr.happy", "room":"1.3", "duration":"90 mins"},
    {"id":"2","name":"Biology", "lecturer":"Mr.happy", "room":"1.3", "duration":"90 mins"},
    {"id":"3","name":"Theology", "lecturer":"Mr.happy", "room":"1.3", "duration":"90 mins"},
    {"id":"4","name":"Sport", "lecturer":"Mr.happy", "room":"1.3", "duration":"90 mins"},
    {"id":"5","name":"Art", "lecturer":"Mr.happy", "room":"1.3", "duration":"90 mins"}
  ]
}
