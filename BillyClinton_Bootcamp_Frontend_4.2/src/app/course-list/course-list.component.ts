import { Component, OnInit } from '@angular/core';
import { CourseService } from '../Service/course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  constructor(private course:CourseService) { }

  ngOnInit() {
  }
  id:string = "";
  name:string = "";
  lecturer:string="";
  room:string="";
  duration:string="";
  

  AddCourse(){
    this.course.CourseList.push({"id":this.id, "name":this.name, "lecturer":this.lecturer, "room":this.room, "duration":this.duration})
    this.id = "";
    this.name ="";
    this.lecturer="";
    this.room="";
    this.duration="";
  }
}
