<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\student;

class studentController extends Controller
{
    function addData(request $request)
    {
        DB::beginTransaction();
        try{
            $this->validate($request, [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|max:255'
        ]);
            $student= new transaction;
            $student->name = $request->input('name');
            $student->address= $request->input('address');
            $student->email= $request->input('email');
            $student->save();
            return response()->json( $student , 201);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}